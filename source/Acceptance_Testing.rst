******************
Acceptance testing
******************

OLAT uses Arquillian Drone (Selenium) for acceptance
testing. A set of acceptance tests is shipped with OLAT..

The full documentation for running the acceptance test can be
found in the OpenOlat README: https://github.com/openolat/openolat#automated-tests

Maven is configured to run all tests in the selenium test directory:

.. code-block:: xml

    <include>org/olat/selenium/*Test.java</include>



Debugging hints
===============

1. To mark an HTML element with a red border use the following snippet:

.. raw:: html

    <script src='https://gitlab.com/snippets/1964086.js'></script>
