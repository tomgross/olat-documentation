************
Architecture
************

Request handling
================

OLAT uses tomcat as application server. Request dispatching is handled by `org.olat.core.servlets.OpenOLATServlet`.
It is responsible for:

 1. Attaching and removing i18n information to the request
 2. Commiting to database
 3. Dispatching to specialized servlets (i.e. WebDAV)


Localization and Internationalization
=====================================

OLAT utilises Java `.properties` files for GUI translations. They are placed
in ``<MODULE>/_i18n/LocalStrings_<LANGCODE>.properties`` 
files. The file

``src/main/java/org/olat/login/_i18n/LocalStrings_en.properties``

contains the English labels for the OLAT login module.

.. note:: Make sure the `.properties` files are encoded ISO 8859-1. If you change a
   translation make sure you run the `native2ascii` utility to transform special characters 
   like umlauts to the correct encoding. (see the definition of the 
   `Properties class <https://docs.oracle.com/javase/8/docs/api/java/util/Properties.html>`_
   for more information)


Customizing translations
------------------------

OLAT provides a neat way to customize translations within the web GUI. To enable this
behavior you have enable translation in the ``olat.local.properties``:

.. code-block:: ini

   is.translation.server=enabled
   # Path to the OLAT project that contains the language properties
   # During development this is normally /workspace/olat/src/main/java
   i18n.application.src.dir=


After restarting tomcat you visit the translation tool of the administration
interface http://localhost:8080/olat/auth/AdminSite/0/translation/0

Here you have several options:

1. **Use the translation tool.** Clicking the *Start* button will open a new browser window 
   with translation statistics and tool. You can search, modify and add translations there.

2. **Use the inline translation.** Clicking the *Enable* button will activate the inline translation
   mode. After you log off and log on again you can translate *most* labels in place, which
   is handy because you have the context of the label.

.. note:: If you customize the translations with on of these methods, the labels in the
   according properties files will be sorted alphabetically. Keep this in mind when you
   encounter big diffs in the originally unsorted property files.

3. **External tool.** You can vist http://localhost:8080/olat/auth/AdminSite/0/i18n/0 you see
   the installed languaes and their translation effort. If you specified the 
   ``i18n.application.src.dir`` in your ``olat.local.properties`` you can export and import
   a language pack for external processing. The format which is used is a JAR-file including
   all properties-files in the given OLAT Java structure. You can unzip the JAR, process the
   translations, zip it (make sure it has a .jar suffix!) and upload it again.


.. code-block:: bash

   zip -r updated-language.jar .

