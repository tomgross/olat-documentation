![Build Status](https://gitlab.com/olatsystems/olat-documentation/badges/master/pipeline.svg)

---

# OLAT Development documentation

https://olatsystems.gitlab.io/olat-documentation/

## Requirements

- Python 3

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
2. python3 -m venv .
3. bin/pip install -r requirements.txt
4. bin/sphinx-build -b html source public
5. cd public
6. bin/python -m http.server

Access at http://localhost:8000/


